from api.serializers import MailingCreateSerializer, MessageSerializer
import requests
import json


def getMailingListStats(mailing):
    messages = mailing.message_mailing_list.all()
    sent_messages = []
    in_process_messages = []
    for message in messages:
        if message.status == 'in_process':
            in_process_messages.append(message)
        elif message.status == 'sent':
            sent_messages.append(message)

    return {
        'mailing_list': MailingCreateSerializer(mailing).data,
        'sent_messages_count': len(sent_messages),
        'in_process_messages_count': len(in_process_messages),
        'sent_messages': MessageSerializer(sent_messages, many=True).data,
        'in_process_messages': MessageSerializer(in_process_messages, many=True).data,
    }




def sendRequestToService(msgId: int, phone: int, text: str):
    header = {
        "Content-Type": "application/json; charset=utf-8",
        "Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTE1Njg1NzMsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6ImRhcnluX2FraSJ9.BJW4aHwCd4j7TmF43TePg_2xXdHqVYJe91kEGuXY3QQ"
    }
    payload = {
        "id": int(msgId),
        "phone": int(phone),
        "text": text
    }
    url = 'https://probe.fbrq.cloud/v1/send/{}'.format(msgId)
    req = requests.post(url, headers=header, data=json.dumps(payload))
    return req

