from django.urls import path
from .views import *


app_name= 'api'


urlpatterns = [
    #Client
    path('create-client/', ClientCreateView.as_view()),
    path('update-and-delete-client/<int:pk>/', ClientUpdateDeleteView.as_view()),

    #Mailing
    path('create-mailing/', MailingCreateView.as_view()),
    path('update-and-delete-mailing/<int:pk>/', MailingUpdateDeleteView.as_view()),
    path('common-stats/', CommonStatsView.as_view()),
    path('detail-stats/<int:pk>/', DetailStatsView.as_view()),

    path('test/', TestView.as_view()),

]
