from django.contrib import admin

from api.models import *

# Register your models here.


admin.site.register(User)
admin.site.register(MailingList)
admin.site.register(Client)
admin.site.register(Message)
admin.site.register(MessageStat)
