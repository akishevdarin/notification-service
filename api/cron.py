from django.utils import timezone
from api.models import *
from api.service import sendRequestToService
import logging

from django.http import HttpResponse


def checker_scheduled_job():
    logger = logging.getLogger('mailing_log')
    print('checker_scheduled_job')
    mailings = MailingList.objects.filter(is_all_sent=False)
    now = timezone.now()
    for mailing in mailings:
        print('ifff: {}'.format(mailing.started_datetime <= now and mailing.end_datetime >= now))
        if mailing.started_datetime <= now and mailing.end_datetime >= now:
            messages = mailing.message_mailing_list.filter(status='in_process')
            is_error = False
            for message in messages:
                #send request
                logger.info("Request sending to service, ID of mailing: {}, ID of message: {}".format(mailing.id, message.id))
                res = sendRequestToService(msgId=message.id, phone=message.sent_to_user.phone, text=mailing.text)
                print('status: {}'.format(res.status_code))
                message.sent_datetime = now
                if res.status_code == 200:
                    logger.info("Request sent to service, ID of mailing: {}, ID of message: {}".format(mailing.id, message.id))
                    message.status = 'sent'
                else:
                    logger.error("Request error to service, ID of mailing: {}, ID of message: {}".format(mailing.id, message.id))
                    message.status = 'error'
                    is_error = True
                message.save()
                # По ходу отправки сообщений должна собираться статистика 
                # (см. описание сущности "сообщение" выше) по каждому 
                # сообщению для последующего формирования отчётов.
                message_stat = MessageStat()
                message_stat.sent_datetime = now
                message_stat.message = message
                message_stat.sent_to_user = message.sent_to_user
                message_stat.save()
            
            if is_error == False:
                mailing.is_all_sent = True
                mailing.save()
            