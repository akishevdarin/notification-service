from django.utils import timezone
from django.db import models
from django.contrib.auth.models import BaseUserManager, AbstractUser



class User(AbstractUser):
    password = models.CharField(max_length=800, null=True, blank=True)
    username = models.CharField(max_length=100, null=True, blank=True)
    last_name = models.CharField(max_length=100, null=True, blank=True)
    phone = models.CharField(max_length=30, unique=True)
    created_at = models.DateTimeField(default=timezone.now)
    last_login = models.DateTimeField(default=timezone.now)
    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['username']
    

class MailingList(models.Model):
    started_datetime = models.DateTimeField(verbose_name='Начало рассылки', default=timezone.now)
    text = models.TextField(verbose_name='Текст рассылки', default='')
    tag = models.CharField(verbose_name='Код мобильного оператора, тег', max_length=50, default='')
    end_datetime = models.DateTimeField(verbose_name='Окончания', default=timezone.now)
    is_all_sent = models.BooleanField(verbose_name='Finished', default=False)


class Client(models.Model):
    phone = models.CharField(verbose_name='Номер', max_length=50, unique=True)
    mobile_operator_code = models.CharField(verbose_name='Код мобильного оператора', max_length=50, default='')
    tag = models.CharField(verbose_name='Тег', max_length=50, default='')
    time_zone = models.CharField(verbose_name='Часовой пояс', max_length=50, default='UTC')


class Message(models.Model):
    sent_datetime = models.DateTimeField(verbose_name='Дата и время отправки', null=True, blank=True)
    status = models.CharField(verbose_name='Тег', max_length=50, default='in_process')#in_process, sent
    mailing_list = models.ForeignKey(MailingList, on_delete=models.CASCADE, related_name='message_mailing_list')
    sent_to_user = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='message_user')


class MessageStat(models.Model):
    sent_datetime = models.DateTimeField(verbose_name='Дата и время отправки', null=True, blank=True)
    message = models.ForeignKey(Message, on_delete=models.SET_NULL, null=True, blank=True, related_name='message_stat_message')
    sent_to_user = models.ForeignKey(Client, on_delete=models.CASCADE, related_name='message_stat_user')