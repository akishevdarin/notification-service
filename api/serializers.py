from rest_framework import serializers
from api.models import *


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class MailingCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MailingList
        fields = '__all__'


class MessageSerializer(serializers.ModelSerializer):
    sent_to_user = ClientSerializer()
    class Meta:
        model = Message
        fields = '__all__'