from django.shortcuts import render
from api.cron import checker_scheduled_job
from api.models import *
from api.serializers import *
from api.service import getMailingListStats
from rest_framework import generics
from rest_framework.response import Response
from django.utils import timezone
from rest_framework.views import APIView
import logging

# Create your views here.


class ClientCreateView(generics.CreateAPIView):
    serializer_class = ClientSerializer
    def perform_create(self, serializer):
        serializer.save()
        logger = logging.getLogger('client_log')
        logger.info("Client created: {}".format(serializer.data['id']))


class ClientUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    def perform_update(self, serializer):
        serializer.save()
        logger = logging.getLogger('client_log')
        logger.info("Client updated: {}".format(serializer.data['id']))
    
    def perform_destroy(self, instance):
        logger = logging.getLogger('client_log')
        logger.info("Client deleted: {}".format(instance.id))
        instance.delete()

class MailingCreateView(generics.CreateAPIView):
    serializer_class = MailingCreateSerializer
    def perform_create(self, serializer):
        user_ids = self.request.data.get("user_ids")
        serializer.save()

        new_mailing = MailingList.objects.filter(id=serializer.data['id']).first()
        
        if user_ids:
            for user_id in user_ids:
                client = Client.objects.filter(id=user_id).first()
                if client:
                    new_message = Message()
                    new_message.mailing_list = new_mailing
                    new_message.sent_to_user = client
                    # new_message.sent_datetime = timezone.now()
                    new_message.save()


class MailingUpdateDeleteView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = MailingCreateSerializer
    queryset = MailingList.objects.all()
    def perform_update(self, serializer):
        user_ids = self.request.data.get("user_ids")
        serializer.save()

        new_mailing = MailingList.objects.filter(id=serializer.data['id']).first()
        
        if user_ids:
            Message.objects.filter(mailing_list=new_mailing).delete()
            for user_id in user_ids:
                client = Client.objects.filter(id=user_id).first()
                if client:
                    new_message = Message()
                    new_message.mailing_list = new_mailing
                    new_message.sent_to_user = client
                    new_message.save()


class CommonStatsView(APIView):
    def get(self, request):
        mailing_lists = MailingList.objects.all()
        list = []
        for mailing_list in mailing_lists:
            list.append(getMailingListStats(mailing_list))

        return Response({
            'data': list
        }, status=200)


class DetailStatsView(APIView):
    def get(self, request, pk):
        query = MailingList.objects.all()
        mailing_list = generics.get_object_or_404(query, id=pk)
        return Response({
            'data': getMailingListStats(mailing_list)
        }, status=200)


class TestView(APIView):
    def get(self, request):
        checker_scheduled_job()
        return Response({
            'res': 'es'
        }, status=200)